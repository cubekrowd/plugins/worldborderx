# WorldBorderX

**Notice**: Starting from 1.18, vanilla itself prevents portals from being created outside the world border, so there's no need to use this plugin.

A small plugin that prevents Nether portals from being created outside the world border.
