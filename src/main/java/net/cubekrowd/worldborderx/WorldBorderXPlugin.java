package net.cubekrowd.worldborderx;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class WorldBorderXPlugin extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerPortal(PlayerPortalEvent e) {
        if (!e.getTo().getWorld().getWorldBorder().isInside(e.getTo())) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(ChatColor.RED + "The linked portal is outside the world border. We cannot teleport you there!");
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onEntityPortal(EntityPortalEvent e) {
        if (!e.getTo().getWorld().getWorldBorder().isInside(e.getTo())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPortalCreate(PortalCreateEvent e) {
        if (e.getReason() != PortalCreateEvent.CreateReason.NETHER_PAIR) {
            return;
        }

        for (var b : e.getBlocks()) {
            var loc = b.getBlock().getLocation();

            if (loc.getWorld() != null && !loc.getWorld().getWorldBorder().isInside(loc)) {
                // cancel the event
                e.setCancelled(true);

                if (e.getEntity() instanceof Player) {
                    e.getEntity().sendMessage(ChatColor.RED + "Cannot create a portal on the other side, because it would be outside the world border!");
                }
                break;
            }
        }
    }
}
